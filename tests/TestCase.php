<?php
/**
 * Made with love by Omar Rida
 * Email: omar@realblocks.com
 * Date: 10/10/19
 */

namespace Tests;


use RealBlocks\IdentityMind\IdentityMind;

/**
 * Class TestCase
 * @package Tests
 */
class TestCase extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public const USERNAME = 'test-username';
    /**
     *
     */
    public const PASSWORD = 'test-password';

    /**
     * @param string $username
     * @param string $password
     * @param null $client
     * @param string $env
     * @return IdentityMind
     */
    protected function identityMind($client = null, $env = 'staging'): IdentityMind
    {
        return new IdentityMind(
            self::USERNAME,
            self::PASSWORD,
            $client,
            $env
        );
    }
}