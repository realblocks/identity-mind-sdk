<?php
/**
 * Made with love by Omar Rida
 * Email: omar@realblocks.com
 * Date: 10/10/19
 */

namespace Tests;


use Mockery;
use stdClass;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use RealBlocks\IdentityMind\IdentityMind;
use RealBlocks\IdentityMind\Exceptions\FailedActionException;

class IdentityMindTest extends TestCase
{
    /** @test */
    function creates_new_instance_from_keys()
    {
        $identityMind = $this->identityMind();

        $this->assertInstanceOf(IdentityMind::class, $identityMind);
    }

    /** @test */
    function creates_new_instance_from_keys_and_client()
    {
        $identityMind = $this->identityMind(new Client);

        $this->assertInstanceOf(IdentityMind::class, $identityMind);
    }

    /** @test */
    function default_environment_is_staging()
    {
        $identityMind = $this->identityMind();

        $this->assertEquals('staging', $identityMind->env());
    }

    /** @test */
    function can_set_specific_environment_for_identity_mind_api()
    {
        $identityMind = $this->identityMind(null, 'sandbox');

        $this->assertEquals('sandbox', $identityMind->env());
    }

    /** @test
     * @throws FailedActionException
     */
    function makes_http_requests()
    {
        $mockHttp = Mockery::mock(Client::class);

        $requestBody = ['name' => 'Omar Rida'];

        $mockHttp->expects('request')
            ->with('POST', 'account/consumer?graphScoreResponse=false', [RequestOptions::JSON => $requestBody])
            ->andReturn(
                $mockResponse = Mockery::mock(ResponseInterface::class)
            )->once();

        $mockResponse->expects('getStatusCode')
            ->andReturn(200)
            ->once();

        $mockResponse->expects('getBody')
            ->andReturn(
                json_encode(['data' => 'value'])
            )->once();

        $response = $this->identityMind($mockHttp)->submit($requestBody);

        $this->assertEquals($response, ['data' => 'value']);
    }

    /** @test */
    function handles_bad_requests()
    {
        $mockHttp = Mockery::mock(Client::class);

        $requestBody = ['bad-field' => 'bad-value'];

        $mockHttp->expects('request')
            ->with('POST', 'account/consumer?graphScoreResponse=false', [RequestOptions::JSON => $requestBody])
            ->andReturn(
                $mockResponse = Mockery::mock(ResponseInterface::class)
            )->once();

        $mockResponse->expects('getStatusCode')
            ->andReturn(400)
            ->once();

        $mockResponse->expects('getBody')
            ->andReturn(json_encode(['error_message' => 'Error!']))
            ->once();

        try {
            $this->identityMind($mockHttp)->submit($requestBody);
        } catch (FailedActionException $exception) {
            $this->assertEquals('Error!', $exception->getMessage());
        }
    }

    /** @test */
    function parses_callback_requests()
    {
        $callbackBody = $this->fakeCallback();

        $parsedCallback = IdentityMind::parseCallback($callbackBody);

        $this->assertTrue($parsedCallback->isApproved());
        $this->assertFalse($parsedCallback->isRejected());
        $this->assertEquals('some-unique-idmind-id',  $parsedCallback->id());
    }

    private function fakeCallback()
    {
        $response = new stdClass;
        $response->state = 'A';
        $response->tid = 'some-unique-idmind-id';

        return $response;
    }
}