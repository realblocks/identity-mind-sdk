# Identity Mind SDK

PHP SDK to simplify interactions with the Identity Mind API.

## Supported Endpoints

[x] POST Create Consumer KYC Evaluation: https://sandbox.identitymind.com/im/account/consumer

## Features

[ ] Parse Identity Mind callback request bodies into easy-to-use value objects. Helper functions could include `approved()` and `rejected()` or `getId()`.

## Contribution Guide

Reach out to me on Slack or email <omar@realblocks.com> if you want to propose changes to the SDK.
