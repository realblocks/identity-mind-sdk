<?php
/**
 * Made with love by Omar Rida
 * Email: omar@realblocks.com
 * Date: 10/12/19
 */

namespace RealBlocks\IdentityMind;


use RealBlocks\IdentityMind\Exceptions\FailedActionException;

trait SubmitsKycEvaluation
{
    /**
     * @param $payload
     * @return string|void
     * @throws FailedActionException
     */
    public function submit($payload)
    {
        return $this->post('account/consumer?graphScoreResponse=false', $payload);
    }
}