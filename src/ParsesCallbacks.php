<?php


namespace RealBlocks\IdentityMind;


trait ParsesCallbacks
{
    public static function parseCallback($request)
    {
        return new CallbackRequest($request);
    }
}