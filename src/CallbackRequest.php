<?php


namespace RealBlocks\IdentityMind;


class CallbackRequest
{
    private $id;
    private $status;

    public function __construct($request)
    {
        $this->id = $request->tid;
        $this->status = $request->state;
    }

    public function id()
    {
        return $this->id;
    }

    public function isApproved()
    {
        return $this->status === 'A';
    }

    public function isRejected()
    {
        return $this->status === 'D';
    }
}