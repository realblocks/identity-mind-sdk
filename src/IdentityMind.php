<?php
/**
 * Made with love by Omar Rida
 * Email: omar@realblocks.com
 * Date: 10/10/19
 */

namespace RealBlocks\IdentityMind;


use GuzzleHttp\Client;

/**
 * Class IdentityMind
 * @package RealBlocks\IdentityMind
 */
class IdentityMind
{
    use ParsesCallbacks;
    use MakesHttpRequests;
    use SubmitsKycEvaluation;

    /**
     * @var Client $client
     */
    private $client;
    /**
     * @var string
     */
    private $env;

    /**
     * IdentityMind constructor.
     * @param $username
     * @param $password
     * @param null $client
     * @param string $env
     */
    public function __construct($username, $password, $client = null, $env = null)
    {
        $this->setEnv($env);

        $this->setClient($client, [$username, $password]);
    }

    /**
     * The subdomain to be used when building the IdentityMind url.
     * By default, this is set to 'staging'. Value can be any of
     * ['sandbox', 'staging', 'edna'] at time of writing this.
     *
     * @param $env
     */
    private function setEnv($env)
    {
        $this->env = $env ?: 'staging';
    }

    /**
     * @return string
     */
    public function env()
    {
        return $this->env;
    }

    /**
     * @param $client
     * @param $credentials
     */
    private function setClient($client, $credentials)
    {
        $this->client = $client ?: new Client([
            'base_uri' => "https://{$this->env}.identitymind.com/im/",
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'auth' => $credentials
        ]);
    }
}