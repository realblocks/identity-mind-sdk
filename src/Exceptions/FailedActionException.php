<?php
/**
 * Made with love by Omar Rida
 * Email: omar@realblocks.com
 * Date: 10/11/19
 */

namespace RealBlocks\IdentityMind\Exceptions;


class FailedActionException extends \Exception
{

}