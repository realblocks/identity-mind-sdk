<?php
/**
 * Made with love by Omar Rida
 * Email: omar@realblocks.com
 * Date: 10/11/19
 */

namespace RealBlocks\IdentityMind;


use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use RealBlocks\IdentityMind\Exceptions\FailedActionException;

/**
 * Trait MakesHttpRequests
 * @package RealBlocks\IdentityMind
 */
trait MakesHttpRequests
{
    /**
     * @param $uri
     * @param $payload
     * @return string|void
     * @throws FailedActionException
     */
    private function post($uri, $payload)
    {
        return $this->request('POST', $uri, $payload);
    }

    /**
     * @param $method
     * @param $uri
     * @param array $payload
     * @return string|void
     * @throws FailedActionException
     */
    private function request($method, $uri, array $payload)
    {
        /** @var ResponseInterface $response */
        $response = $this->client->request($method, $uri,
            empty($payload) ? [] : [RequestOptions::JSON => $payload]
        );

        if ($response->getStatusCode() !== 200) {
            $this->handleRequestError($response);
        }

        $responseBody = (string) $response->getBody();
        return json_decode($responseBody, true) ?: $responseBody;
    }

    /**
     * @param ResponseInterface $response
     * @throws FailedActionException
     */
    private function handleRequestError(ResponseInterface $response)
    {
        $errorMessage = $this->getErrorMessage($response);

        throw new FailedActionException((string) $errorMessage, $response->getStatusCode());
    }

    private function getErrorMessage(ResponseInterface $response)
    {
        return json_decode($response->getBody(), true)['error_message'];
    }
}